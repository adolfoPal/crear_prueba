<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Matricula electivas</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="./">Inicio</a></li>
            <?php if($_SESSION['tipo']==1){?>
              <li><a href="./php/agregar_electiva.php">Agregar Electivas</a></li>
            <?php } ?>
            <li><a href="./php/api.php?action=logout">Salir</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-12 main">
          <h1 class="page-header">Hola <?php echo $_SESSION['usuario']; ?></h1>
          <h2 class="sub-header">Listado Electivas</h2>
          <div class="table-responsive">
            <table class="table table-striped" id="electivaTable">
              <thead>
                <tr>
                  <th>Electiva</th>
                  <th>Descripcion</th>
                  <th>Profesor</th>
                  <th>Cupos</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody id="electivasTableBody">
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!--MODAL EDITAR ELECTIVAS-->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Editar Electiva</h4>
          </div>
          <div class="modal-body">
            <form class="form-signin" method="post" action="./php/api.php?action=editElectiva">
                <input type="hidden" id="idElectiva" name="idElectiva">            
                <label for="nombre" class="sr-only">nombre</label>
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="" autofocus="">
                <label for="codigo" class="sr-only">descripcion</label>
                <textarea class="form-control" rows="5" id="descripcion" maxlength="50" name="descripcion" placeholder="Descripcion" required=""></textarea>
                <label for="profesor" class="sr-only">Profesor</label>
                <input type="text" id="profesor" name="profesor" class="form-control" placeholder="Profesor" required="">
                <label for="cupos" class="sr-only">Cupos</label>
                <input type="text" id="cupos" name="cupos" class="form-control" placeholder="Cupos" required="">
              <button class="btn btn-lg btn-primary btn-block" type="submit">Modificar</button>
              <button type="button" class="btn btn-lg btn btn-default btn-block" data-dismiss="modal">Cancelar</button>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--MODAL MATRICULADOS-->
    <div class="modal fade" id="matriculadosModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Estudiantes matriculados</h4>
          </div>
          <div class="modal-body">
            <div id="listaEstudiantes"></div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
<script src="./js/template.js"></script>