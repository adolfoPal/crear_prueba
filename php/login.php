<body>
    <div class="container">
        <form class="form-signin" method="post" action="./php/api.php?action=login">
            <h2 class="form-signin-heading">Iniciar sesion</h2>
            <label for="inputEmail" class="sr-only">Codigo</label>
            <input type="text" id="codigo" name="codigo" class="form-control" placeholder="Codigo" required="" autofocus="">
            <label for="inputPassword" class="sr-only">Contrase&ntilde;a</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Contrase&ntilde;a" required="">
            <button class="btn btn-lg btn-primary btn-block" type="submit">ingresar</button>
            <a class="btn btn-lg btn-default btn-block" href="./php/registro.php">Registrarse</a>
        </form>
    </div> <!-- /container -->
</body>