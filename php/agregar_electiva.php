<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <form class="form-signin" method="post" action="./api.php?action=addElectiva">
                <h2 class="form-signin-heading">Agregar Electiva</h2>
                <label for="nombre" class="sr-only">nombre</label>
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="" autofocus="">
                <label for="codigo" class="sr-only">descripcion</label>
                <textarea class="form-control" rows="5" id="descripcion" maxlength="50" name="descripcion" placeholder="Descripcion" required=""></textarea>
                <label for="profesor" class="sr-only">Profesor</label>
                <input type="text" id="profesor" name="profesor" class="form-control" placeholder="Profesor" required="">
                <label for="cupos" class="sr-only">Cupos</label>
                <input type="text" id="cupos" name="cupos" class="form-control" placeholder="Cupos" required="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Agregar</button>
                <a class="btn btn-lg btn-default btn-block" href="../">Volver</a>
            </form>
        </div> <!-- /container -->
    </body>
</html>