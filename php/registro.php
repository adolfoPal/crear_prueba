<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <form class="form-signin" method="post" action="./api.php?action=addUser">
                <h2 class="form-signin-heading">Registro</h2>
                <label for="nombre" class="sr-only">nombre</label>
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="" autofocus="">
                <label for="codigo" class="sr-only">codigo</label>
                <input type="text" id="codigo" name="codigo" class="form-control" placeholder="Codigo" required="" autofocus="">
                <label for="password" class="sr-only">Contrase&ntilde;a</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Contrase&ntilde;a" required="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
            </form>
        </div> <!-- /container -->
    </body>
</html>