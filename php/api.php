<?php
	$db_error="";
	$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
	mysqli_select_db($db_config, 'crear_digital') or die( $db_error );

	$action = $_REQUEST['action'];

	if(isset($_POST["nombre"])){
		$nombre = $_POST['nombre'];
	}
	if(isset($_POST["codigo"])){
		$codigo = $_POST['codigo'];
	}
	if(isset($_POST["password"])){
		$password = md5($_POST['password']);
	}
	if(isset($_POST["profesor"])){
		$profesor = $_POST['profesor'];
	}
	if(isset($_POST["descripcion"])){
		$descripcion = $_POST['descripcion'];
	}
	if(isset($_POST["cupos"])){
		$cupos = $_POST['cupos'];
	}
	if(isset($_POST["idElectiva"])){
		$idElectiva = $_POST['idElectiva'];
	}
	if(isset($_POST["idUsuario"])){
		$idUsuario = $_POST['idUsuario'];
	}
	if(isset($_GET["profesor"])){
		$profesor = $_GET['profesor'];
	}
	if(isset($_GET["descripcion"])){
		$descripcion = $_GET['descripcion'];
	}
	if(isset($_GET["cupos"])){
		$cupos = $_GET['cupos'];
	}
	if(isset($_GET["idElectiva"])){
		$idElectiva = $_GET['idElectiva'];
	}
	if(isset($_GET["nombre"])){
		$nombre = $_GET['nombre'];
	}
	if(isset($_GET["idUsuario"])){
		$idUsuario = $_GET['idUsuario'];
	}

	switch($action){
		case "addUser":
			addUser($nombre, $codigo, $password);
		break;
		case "getUser":
			getUser();
		break;
		case "logout":
			logout();
		break;
		case "login":
			login();
		break;
		case "addElectiva":
			addElectiva($nombre, $descripcion, $profesor, $cupos);
		break;
		case "getElectivas":
			getElectivas();
		break;
		case "getElectiva":
			getElectiva($idElectiva);
		break;
		case "editElectiva":
			editElectiva($idElectiva,$nombre, $descripcion, $profesor, $cupos);
		break;
		case "borrarElectiva":
			borrarElectiva($idElectiva);
		break;
		case "matricular":
			matricular($idElectiva, $idUsuario);
		break;
		case "getUsuariosMatriculados":
			getUsuariosMatriculados($idElectiva);
		break;		
	}

	function getUsuariosMatriculados($idElectiva){
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );

		$query  = 'SELECT * FROM matricula INNER JOIN usuario ON matricula.id_estudiante = usuario.id WHERE id_electiva ="'.$idElectiva.'"';
		$exec = mysqli_query($db_config, $query);
		$data = array();

		if(mysqli_query($db_config, $query)){
			while($result = mysqli_fetch_object($exec)){
				$data[] = $result;
			}
		}
		echo json_encode($data);
	}

	function matricular($idElectiva, $idUsuario){
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'UPDATE electiva SET cupos= cupos-1 WHERE id = "'.$idElectiva.'"';
		$queryUsuario = 'INSERT INTO `matricula` (id_estudiante, id_electiva) VALUES ("'.$idUsuario.'","'.$idElectiva.'")';
		mysqli_query($db_config, $queryUsuario);
		mysqli_query($db_config, $query);
	}

	function addUser($nombre, $codigo, $password){
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'INSERT INTO usuario (nombre, tipo, codigo, password) VALUES ("'.$nombre.'","2","'.$codigo.'","'.$password.'")';
		mysqli_query($db_config, $query);
		print "<script>alert(\"Usuario creado correctamente, proceda a iniciar sesion\");window.location='../';</script>";
	}

	function addElectiva($nombre, $descripcion, $profesor, $cupos){
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'INSERT INTO electiva (nombre, descripcion, profesor, cupos) VALUES ("'.$nombre.'","'.$descripcion.'","'.$profesor.'","'.$cupos.'")';
		mysqli_query($db_config, $query);
		print "<script>alert(\"Electiva creada correctamente\");window.location='../';</script>";		
	}

	function getElectiva($idElectiva){
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'SELECT * FROM `electiva` WHERE `id` ='.$idElectiva;
		$exec = mysqli_query($db_config, $query);
		$data = '';

		if(mysqli_query($db_config, $query)){
			while($result = mysqli_fetch_object($exec)){
				$data = $result;
			}
		}
		echo json_encode($data);
	}
	
	function editElectiva($idElectiva, $nombre, $descripcion, $profesor, $cupos){
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'UPDATE electiva SET nombre="'.$nombre.'", descripcion="'.$descripcion.'", profesor="'.$profesor.'", cupos="'.$cupos.'" WHERE id = "'.$idElectiva.'"';
		mysqli_query($db_config, $query);
		print "<script>alert(\"Electiva modificada correctamente\");window.location='../';</script>";		
	}

	function borrarElectiva($idElectiva){
		$idElectiva = $_POST['idElectiva'];
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'DELETE FROM electiva WHERE id = "'.$idElectiva.'"';
		mysqli_query($db_config, $query);
	}
	
	function getUser(){
		session_start();
		header("Content-type: application/json");
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'SELECT * FROM `usuario` WHERE `codigo` ='.$_SESSION["codigo"];
		$exec = mysqli_query($db_config, $query);
		$data = array();

		if(mysqli_query($db_config, $query)){
			while($result = mysqli_fetch_object($exec)){
				$data[] = $result;
			}
		}
		echo json_encode(array($data));
	}

	function getElectivas(){
		header("Content-type: application/json");
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		$query  = 'SELECT * FROM `electiva`';
		$exec = mysqli_query($db_config, $query);
		$data = array();

		if(mysqli_query($db_config, $query)){
			while($result = mysqli_fetch_object($exec)){
				$data[] = $result;
			}
		}
		echo json_encode(array($data));
	}

	function login(){
		session_start();  
    	$codigo = $_POST['codigo'];  
    	$password = md5($_POST['password']); 

    	$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );  
    	$query = "SELECT * FROM usuario WHERE codigo='$codigo' AND password='$password'";  
    	$exec = mysqli_query($db_config, $query);  
    	$num_row = mysqli_num_rows($exec);  
    	$row=mysqli_fetch_array($exec);  
    
    	if( $num_row >=1 ){  
        	$_SESSION['usuario']=$row['nombre'];
			$_SESSION['codigo']=$row['codigo'];
			$_SESSION['tipo']=$row['tipo'];
        	$_SESSION['estado']= 'Autenticado';
			header('Location: ../');
    	}  
		else{  
			print "<script>alert(\"Datos incorrectos. Verifique sus datos e intente logearse nuevamente\");window.location='../';</script>";
			} 
	}

	function logout(){
		session_start();
		$db_config = mysqli_connect('localhost','root','','crear_digital') or die( $db_error );
		unset($_SESSION);
		session_destroy();
		mysqli_close($db_config);		
		header('Location: ../');
		die();
	}

?>