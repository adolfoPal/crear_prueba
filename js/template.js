function getElectivas(){
    //Obtiene las Electivas
    $.ajax({
        url: "./php/api.php?action=getElectivas",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        success: function(result){
            var electivas = result[0];
            setTimeout(function(){
                if(sessionStorage.userTipo == '1'){
                    for(var i = 0; i< electivas.length; i++){
                        document.getElementById("electivasTableBody").innerHTML += '<tr><td>'+electivas[i].nombre+'</td><td>'+electivas[i].descripcion+'</td><td>'+electivas[i].profesor+'</td><td>'+electivas[i].cupos+'</td><td><a class="btn btn-default" data-toggle="modal" data-target="#editModal" onClick="getElectiva('+electivas[i].id+')"><span class="glyphicon glyphicon-edit"></span></a><a class="btn btn-default" onClick="borrarElectiva('+electivas[i].id+')"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';  
                    }
                }
                if(sessionStorage.userTipo == '2'){
                    for(var i = 0; i< electivas.length; i++){
                        document.getElementById("electivasTableBody").innerHTML += '<tr><td>'+electivas[i].nombre+'</td><td>'+electivas[i].descripcion+'</td><td>'+electivas[i].profesor+'</td><td>'+electivas[i].cupos+'</td><td><a class="btn btn-default"><span class="glyphicon glyphicon-check" onClick="matricular('+electivas[i].id+','+sessionStorage.userId+')"></span></a><a class="btn btn-default" onClick="getUsuariosMatriculados('+electivas[i].id+')" data-toggle="modal" data-target="#matriculadosModal"><span class="glyphicon glyphicon-eye-open"></span></a></td></tr>';  
                    }
                }
                $('#electivaTable').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    },
                    "dom": '<"top"f>rt<"bottom"lpi><"clear">',
                    "destroy": true
                });
            },200);	
        },error: function(error){
            console.log(error);
        }
    });    
    //Fin
}

function borrarElectiva(idElectiva){
    var electivaSel = 'idElectiva='+idElectiva;
    $.ajax({
        url: "./php/api.php?action=borrarElectiva",
        method: "POST",
        data: electivaSel,
        success: function() {
           window.location='./';
        }
    });
    return false;
}

function getElectiva(electivaSel){
    $.ajax({
        url: "./php/api.php?action=getElectiva&idElectiva="+electivaSel,
        contentType: "application/json; charset=utf-8",
        type: "GET",
        success: function(result){
            var electivaSeleccionada = JSON.parse(result);
            console.log(electivaSeleccionada);
            $('#idElectiva').val(electivaSeleccionada.id);
            $('#nombre').val(electivaSeleccionada.nombre);
            $('#descripcion').val(electivaSeleccionada.descripcion);
            $('#profesor').val(electivaSeleccionada.profesor);
            $('#cupos').val(electivaSeleccionada.cupos);
        },error: function(error){
            console.log(error);
        }
    });
}

function matricular(idElectiva, idUsuario){
    var dataString = '&idElectiva='+idElectiva+"&idUsuario="+idUsuario;
    $.ajax({
        url: "./php/api.php?action=matricular"+dataString,
        type: "GET",
        success: function(){
            window.location='./';
        },error: function(error){
            console.log(error);
        }
    });
    return false;
}

function getUsuariosMatriculados(idElectiva){
    var electivaSel = 'idElectiva='+idElectiva;
    $.ajax({
        url: "./php/api.php?action=getUsuariosMatriculados",
        method: "POST",
        data: electivaSel,
        success: function(result) {
            var matriculados = JSON.parse(result);
            $('#listaEstudiantes').html('');
            console.log(matriculados);
            for(var i = 0; i< matriculados.length; i++){
                document.getElementById("listaEstudiantes").innerHTML += '<p>'+matriculados[0].codigo+' - '+matriculados[0].nombre+'</p>';  
            }
        }
    });
    return false;
}

$(document).ready(function() {
    getElectivas();
    //Obtiene el Usuario Actual
    $.ajax({
        url: "./php/api.php?action=getUser",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        success: function(result){
            var usuario = result[0][0];
            sessionStorage.userId = usuario.id;
            sessionStorage.userCodigo = usuario.codigo;
            sessionStorage.userNombre = usuario.nombre;
            sessionStorage.userTipo = usuario.tipo;
        },error: function(error){
            console.log(error);
        }
    });
    //Fin
});